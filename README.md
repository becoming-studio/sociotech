# SOCIOTECH

curriculum for a semester course in socio-technical entanglements

telegram group: https://t.me/sociotech


## Structure

* ActorNetwork Theory
* Platform Capitalism
* Cybernetic Ideology
* Black Boxing
* Infrastructure
* Cloud
* Iot
* Interfaces
* Users
---
* Git technology. Console vs browser interfaces.
* Extract algorithms from your daily routines. Flow diagrams.
* History of computing machines. Turing completeness.
* Markup languages. XML. Html+Css. DOM. Websites.
* Publishing a website thorugh a git platform
* Js scripting. Js vs DOM using P5
* HTML5 canvas vs P5.
* Networks & internet technologies. 
* Server-client vs mesh 
* ip | tcp/udp  &  tools

## Resources

* Hardware
* Software
  - firefox
  - atom / sublime text
  - P5
  - Processing 

## Participants 2018
* [Judith](https://github.com/Judith89/ejercicios/wiki) 
* [Aritz](https://github.com/arkin95/exercicis-sociotech/wiki)
* [Berta](https://github.com/Berta15/hola/wiki)
* [Alex](https://github.com/alexwifi64/alexwifi/)
* [Jon](https://github.com/jonimelavo/exercici-intractius/wiki)
* [Max](https://github.com/maxazc/sociotech/wiki)
* [Ada](https://github.com/adafontecilla/Sociotech)
* [Meritxell](https://github.com/Mtxell/exercicis-sociotech)
* [Mireia](https://github.com/mireiavillanueva96/sociotech)
* [Mariona]
* [Berta]
* [Laura]
* [Aina](https://github.com/AinaCotado)
* [Andreu](https://github.com/AndreuIII)
* [Raul](https://github.com/pratipo)